from django.test import TestCase, Client, LiveServerTestCase
from selenium import webdriver
from time import sleep
from .models import RegisteredUser


# Create your tests here.
class DjangoTest(TestCase):
    def test_home_page(self):
        response = Client().get('')
        self.assertEquals(200, response.status_code)

    def test_register_form(self):
        response = Client().get('/register/')
        self.assertEquals(200, response.status_code)

    def test_form_saran(self):
        response = Client().get('/formsaran/')
        self.assertEquals(200, response.status_code)

    def test_buku(self):
        response = Client().get('/buku/')
        self.assertEquals(200, response.status_code)

    def test_generate_buku_json(self):
        response = Client().get('/generatebuku/', {"search": "quilting"})
        self.assertEquals(200, response.status_code)

    def test_add_user(self):
        count = RegisteredUser.objects.all().count()
        Client().post('/register/register/', {"email": "test@test.com", "password": "passwordtest", "name": "nametest", "birthdate": "1000-01-01"})
        self.assertEquals(count+1, RegisteredUser.objects.all().count())

    def test_delete_user(self):
        Client().post('/register/register/', {"email": "test@test.com", "password": "passwordtest", "name": "nametest", "birthdate": "1000-01-01"})
        user = RegisteredUser.objects.all().get(email="test@test.com")
        Client().post('/register/deleteuser/', {"pk": user.pk, "password": "passwordtest"})
        self.assertEquals(0, RegisteredUser.objects.all().count())


'''
class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super(FunctionalTest, self).setUp()
        chrome_options = webdriver.chrome.options.Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.implicitly_wait(25)
        ''
        # kalo di local ini di uncomment yang atas di comment
        self.browser = webdriver.Chrome()
        self.browser.implicitly_wait(1)
        ''

    def tearDown(self):
        self.browser.quit()

    def test_ajax_loaded(self):
        self.browser.get(self.live_server_url + "/buku/")
        self.browser.find_element_by_id("search").send_keys("quilting")
        self.browser.find_element_by_id("show-book").click()
        sleep(10)
        self.assertIn("quilting", self.browser.page_source)

    def test_counter(self):
        self.browser.get(self.live_server_url + "/buku/")
        self.browser.find_element_by_id("search").send_keys("quilting")
        self.browser.find_element_by_id("show-book").click()
        sleep(10)
        star = self.browser.find_elements_by_class_name("fa-star")
        for i in star:
            i.click()
        self.assertEquals(self.browser.find_element_by_id("counter").text,
                          "10")
'''
from django.contrib import admin
from .models import JadwalPribadi, RegisteredUser

# Register your models here.
admin.site.register(JadwalPribadi)
admin.site.register(RegisteredUser)
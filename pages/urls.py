from django.urls import path
from django.contrib.auth.views import LogoutView
from django.conf import settings
from .views import (homePageView, formSaranView, registerView,
                    postJadwal, deleteDatabase, bukuView,
                    generateBukuJson, registerUser, validateUser,
                    getUser, deleteUser, saveToSession)

urlpatterns = [
    path('formsaran/', formSaranView, name='formsaran'),
    path('register/', registerView, name='register'),
    path('register/register/', registerUser, name='register_user'),
    path('register/validate/', validateUser, name='validate_user'),
    path('register/getuser/', getUser, name='get_user'),
    path('register/deleteuser/', deleteUser, name='delete_user'),
    path('jadwalkegiatan/', postJadwal, name='jadwalkegiatan'),
    path('', homePageView, name='home'),
    path('deletedatabase/', deleteDatabase, name='deletedatabase'),
    path('buku/', bukuView, name='buku'),
    path('generatebuku/', generateBukuJson, name='generate_buku'),
    path('auth/logout/', LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
    path('savetosession/', saveToSession, name='save_to_session')
]
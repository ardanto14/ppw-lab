# Generated by Django 2.1.1 on 2018-10-03 09:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0002_auto_20181003_1055'),
    ]

    operations = [
        migrations.RenameField(
            model_name='jadwalpribadi',
            old_name='tanggal',
            new_name='waktu',
        ),
        migrations.AlterField(
            model_name='jadwalpribadi',
            name='kategori',
            field=models.CharField(max_length=30),
        ),
    ]

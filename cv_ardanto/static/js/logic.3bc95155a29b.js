$(document).ready(function() {
    $("#show-book").click(function() {
        $.ajax({
            url: "/generatebuku/",
            data: {"search": $("#search").val()},
            beforeSend: function() {
                $("#table-container").html("processing");
                $("#counter").text("0");
            },
            success: function(data) {
                try {
                    data.items.length;
                } catch (TypeError) {
                    $("#table-container").html("not found");
                    return;
                }
                var volumeInfo;
                var html;
                for (var i = 0; i < data.items.length; i++) {
                    volumeInfo = data.items[i].volumeInfo;
                    html += "<tr>";
                    html += "<td class='align-middle text-center'>" + (i + 1) + "</td>";
                    html += "<td class='align-middle text-center'>";
                    try {
                        volumeInfo.imageLinks.thumbnail;
                        html += "<img src='";
                        html += volumeInfo.imageLinks.thumbnail;
                        html += "'>";
                    } catch (TypeError) {
                        html += "-";
                    }
                    html += "</td>";
                    html += "<td class='align-middle text-center'>" + volumeInfo.title + "</td>";
                    html += "<td class='align-middle text-center'>";
                    try {
                        for (var j = 0; j < volumeInfo.authors.length; j++) {
                            html += volumeInfo.authors[j];
                        }
                    } catch (TypeError) {
                        html += "-";
                    }
                    html += "</td>";
                    html +="<td class='align-middle text-center'><p class='fa fa-star'></p></td>";
                    html += "</tr>";
                }
                $("#table-container").html("<table class='table table-bordered'><thead><tr><th>Nomor</th><th>Gambar</th><th>Judul</th>" +
                    "<th>Penulis</th><th>Favorite</th></tr></thead><tbody id='output'></tbody></table>")
                $("#output").append(html);
                $(".fa-star").click(function() {
                    if ($(this).hasClass("checked")) {
                        $(this).removeClass("checked");
                        var newVal = parseInt($("#counter").text()) - 1
                        $("#counter").text(newVal)
                    } else {
                        $(this).addClass("checked");
                        var newVal = parseInt($("#counter").text()) + 1
                        $("#counter").text(newVal)
                    }
                })
            }
        })
    })
})